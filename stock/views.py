from django.views.generic import ListView, DetailView
from stock.models import Produtos
from django.db.models import Q
from django.views.generic.edit import CreateView
from django.shortcuts import redirect
# Create your views here.

class ProdutosListView(ListView):
    model = Produtos

class SearchView(ListView):
    model = Produtos
    template_name = 'search.html'

    def get_queryset(self):
        query = self.request.GET.get('pesq')
        object_list = Produtos.objects.filter(
            Q(Nome__icontains = query) |
            Q(Descrição__icontains = query) |
            Q(Código__icontains = query))
        return object_list

class ProdutosCreateView(CreateView):
    model = Produtos
    fields = ['Nome', 'Descrição', 'Código', 'Imagem', 'Quantidade']

class SemEstoqueListView(ListView):
    model = Produtos

    def get_queryset(self):
        query = 0
        object_list = Produtos.objects.filter(Quantidade=0)
        return object_list

class AddStockView(DetailView):
    model = Produtos

    def post(self, request, *args, **kwargs):
        produtos = self.get_object()
        Quantidade = self.request.POST['Quantidade']
        produtos.Quantidade += int(Quantidade)
        produtos.save()
        return redirect('Produtos-list')

class RemoveStockView(DetailView):
    model = Produtos

    def post(self, request, *args, **kwargs):
        produtos = self.get_object()
        Quantidade = self.request.POST['Quantidade']
        produtos.Quantidade -= int(Quantidade)
        produtos.save()
        return redirect('Produtos-list')
