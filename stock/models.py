from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Produtos(models.Model):
	Nome = models.CharField(max_length = 50)
	Descrição = models.TextField()
	Código = models.CharField(max_length=50)
	Imagem = models.ImageField(null=True, blank=True)
	Quantidade = models.PositiveIntegerField()

	def __str__(self):
		return self.Código + ' - ' + self.Nome